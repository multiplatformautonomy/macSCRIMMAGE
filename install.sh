#! /bin/bash

echo "Installing dependencies" 
#installing xcode tools


which -s xcode-select
if [[ $? != 0 ]] ; then
    # Install Homebrew
    echo "NO Xcode clt"
    $(xcode-select --install)
else
	echo "xcode-select comandline tools found"
    
fi




which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    echo "Installing Homebrew"
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
	echo "Updating homebrew"
    brew update
fi

echo "Installing dependencies from brew tap crichardson332/crich_brews"

$(brew tap crichardson332/crich_brews)
echo "<-Brew install->"
brew deps --include-build --include-optional scrimmage > deps.txt
echo "deps.txt file created"

xargs brew install < deps.txt
echo "Brew install done"
brew uninstall boost vtk@6.2

#geolib install
echo "Unzip geographic lib. Expecting version 1.50.1 (.tar.gz) file"
$(tar -xzvf GeographicLib-1.50.1.tar.gz)
cd GeographicLib-1.50.1
./configure
mkdir build
cd build
cmake ..
make -j 8
cd ..
cd ..

#boost install
echo "Unzipping boost"
$(tar -xzvf boost_1_58_0.tar.gz)
cd boost_1_58_0
./bootstrap.sh
./b2 -j 8
./b2 install -j 8
cd ..

brew install vtk@6.2

#cloning and building scrimmage

git clone https://github.com/gtri/scrimmage
cd scrimmage 
mkdir build
cd build
cmake ..
make -j 8
source ~/.scrimmage/setup.bash