# macSCRIMMAGE

## Scrimmage - macOS

To build scrimmage from source on macOS, you must have command line tools
installed. If you do not have them, paste this command into a Terminal prompt
and hit enter:

    $ xcode-select --install

## Dependencies

### Install Homebrew

SCRIMMAGE's dependencies are managed using the [Homebrew](https://brew.sh/)
package manager. To install Homebrew, either follow the instructions on the
website or type the following command into a terminal prompt:

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

### Installing dependencies

Homebrew can be used to easily install all of SCRIMMAGE's dependencies. First,
add the SCRIMMAGE homebrew tap. Then install all dependencies:

    $ brew tap crichardson332/crich_brews
    $ brew install $(brew deps --include-build --include-optional scrimmage)

To list info about the SCRIMMAGE homebrew package or just list dependencies
for SCRIMMAGE:

    $ brew info scrimmage
    $ brew deps scrimmage



## Building SCRIMMAGE

### Clone and build

SCRIMMAGE is built with [CMake](https://cmake.org/). Clone down the SCRIMMAGE
repository and build with the standard CMake commands:

    $ git clone https://github.com/gtri/scrimmage
    $ cd /path/to/scrimmage/repo
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ source ~/.scrimmage/setup.bash



## Run

Run an example SCRIMMAGE sim:

    $ cd /path/to/scrimmage/repo
    $ scrimmage missions/straight.xml

Tutorials and examples can be found on the [SCRIMMAGE website.](http://www.scrimmagesim.org/docs/sphinx/html/index.html)


## Debugging 
### GeographicLib link error. 
To fix this error you must download and install GeographicLib from [https://sourceforge.net/projects/geographiclib/files/distrib/](https://sourceforge.net/projects/geographiclib/files/distrib/)

    $ cd into grographiclib 
    $ ./configure
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make


### Boost library errors.
If you run into errors with boost make sure you are using boost 1.58.0. This version can be installed without brew by downloading it from the following link
[Boost version 1.58.0](https://www.boost.org/users/history/version_1_58_0.html) 

You may need to uninstall the version of boost that is installed with homebrew, to do this you have to also uninstall vtk@6.2. The following command will remove boost and vtk@6.2

    $ brew uninstall vtk@6.2 boost 
  
Now to install the version 1.58.0 download the zip file extract it and cd into the folder you must run the following commands


    $./bootstrap.sh
    $./b2
    $./b2 install
After doing this you have to install vtk@6.2 by running 

    $ brew install vtk@6.2